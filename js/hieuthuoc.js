var data2 = [
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "48. Trường Đại học Y Hà Nội",
   "address": "Số 1 Tôn Thất Tùng, Kim Liên, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.003216,
   "Latitude": 105.830662
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "49. Trường Đại học Dược Hà Nội",
   "address": "13-15 Lê Thánh Tông, Phan Chu Trinh, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.021626,
   "Latitude": 105.858524
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "50. Đại học Y - Dược thành phố Hồ Chí Minh",
   "address": "217 Hồng Bàng, Phường 11, Quận 5, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.754942,
   "Latitude": 106.663051
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "51. Trường Đại học Y Dược Hải Phòng",
   "address": "72A Nguyễn Bỉnh Khiêm, Đằng Gia, Ngô Quyền, Hải Phòng, Việt Nam",
   "Longtitude": 20.842863,
   "Latitude": 106.698026
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "52. Trường Đại học Y Dược Thái Bình",
   "address": "373 Lý Bôn, P. Kỳ Bá, TP Thái Bình, Việt Nam",
   "Longtitude": 20.442992,
   "Latitude": 106.339811
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "53. Trường Đại học Y - Dược Cần Thơ",
   "address": "179 Đường Nguyễn Văn Cừ, Phường An Khánh, Ninh Kiều, Cần Thơ , Việt Nam",
   "Longtitude": 10.034934,
   "Latitude": 105.753623
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "54. Trường Đại học Y tế công cộng",
   "address": "1A Đức Thắng, Phường Đức Thắng, Đông Ngạc, Bắc Từ Liêm, Hà Nội, Việt Nam",
   "Longtitude": 21.082785,
   "Latitude": 105.780004
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "55. Trường Đại học Điều dưỡng Nam Định",
   "address": "257 Hàn Thuyên, Vị Xuyên, TP. Nam Định, Nam Định, Việt Nam",
   "Longtitude": 20.437332,
   "Latitude": 106.183319
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "56. Học viện Y - Dược học cổ truyền Việt Nam",
   "address": "2 Trần Phú, P. Mộ Lao, Hà Đông, Hà Nội, Việt Nam",
   "Longtitude": 20.984769,
   "Latitude": 105.792211
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "57. Trường Đại học Kỹ thuật y tế Hải Dương",
   "address": "1 Đường Vũ Hữu, P. Thanh Trung, Thành phố Hải Dương, Hải Dương, Việt Nam",
   "Longtitude": 20.938153,
   "Latitude": 106.305532
 },
 {
   "type": "maps/map-images/gdyk.png",
   "Name": "58. Trường Đại học Kỹ thuật Y - Dược Đà Nẵng",
   "address": "99 Hùng Vương, Hải Châu 1, Hải Châu, Đà Nẵng, Việt Nam",
   "Longtitude": 16.067899,
   "Latitude": 108.218299
 },];