var datatramyte = [
  {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế An Giang",
   "address": "15 Lê Triệu Kiết, Mỹ Bình, tp. Long Xuyên,  An Giang",
   "Longtitude": 10.388661,
   "Latitude": 105.436637
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bà Rịa - Vũng Tàu",
   "address": "Số 1 p, Phạm Văn Đồng, Phước Trung, Bà Rịa, Bà Rịa - Vũng Tàu, Việt Nam",
   "Longtitude": 10.487666,
   "Latitude": 107.180595
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bạc Liêu",
   "address": "Số 07, đường Nguyễn Tất Thành, phường 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": 9.295709,
   "Latitude": 105.732421
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bắc Giang",
   "address": "Đường Hùng Vương, P. Ngô Quyền, TP. Bắc Giang",
   "Longtitude": 21.278976,
   "Latitude": 106.19896
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bắc Kạn",
   "address": "14 Trường Chinh, Phùng Chí Kiên, Thành phố Bắc Kạn, Bắc Kạn, Việt Nam",
   "Longtitude": 22.146664,
   "Latitude": 105.835303
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bắc Ninh",
   "address": "Số 3 Lý Thái Tổ, P. Suối Hoa, TP. Bắc Ninh",
   "Longtitude": 21.18285,
   "Latitude": 106.075203
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bến Tre",
   "address": "39 Đoàn Hoàng Minh, Phường 5, Bến Tre, Việt Nam",
   "Longtitude": 10.238646,
   "Latitude": 106.368035
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bình Dương",
   "address": "211 Yersin, Phú Chánh, Tân Uyên, Bình Dương, Việt Nam",
   "Longtitude": 11.057158,
   "Latitude": 106.68226
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bình Định",
   "address": "756 Trần Hưng Đạo, Đống Đa, Thành phố Qui Nhơn, Bình Định, Việt Nam",
   "Longtitude": 13.78429,
   "Latitude": 109.214527
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bình Phước",
   "address": "1041 Quốc Lộ 14, Tiến Thành, Đồng Xoài, Bình Phước, Việt Nam",
   "Longtitude": 11.527584,
   "Latitude": 106.866469
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Bình Thuận",
   "address": "01 Lê Hồng Phong, Phú Trinh, Thành phố Phan Thiết, Bình Thuận, Việt Nam",
   "Longtitude": 10.930948,
   "Latitude": 108.100676
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Cà Mau",
   "address": "155 Bùi Thị Trường, Phường 5, Thành phố Cà Mau, Cà Mau, Việt Nam",
   "Longtitude": 9.178714,
   "Latitude": 105.1565
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Cao Bằng",
   "address": "31 Đường Hiến Giang, P. Hợp giang, Cao Bằng, Việt Nam",
   "Longtitude": 22.666962,
   "Latitude": 106.2556
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Cần Thơ",
   "address": "71 Lý Tự Trọng, An Cư, Ninh Kiều, Cần Thơ, Việt Nam",
   "Longtitude": 10.035382,
   "Latitude": 105.778519
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Đà Nẵng",
   "address": "Tầng 23 Trung tâm Hành chính thành phố Đà Nẵng, 24 Trần Phú, phường Thạch Thang, quận Hải Châu, thành phố Đà Nẵng",
   "Longtitude": 10.04104,
   "Latitude": 105.77744
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Đắk Lắk",
   "address": "68, Lê Duẩn, Tân Thành, Thành phố Buôn Ma Thuột, Đắk Lắk, Việt Nam",
   "Longtitude": 12.668268,
   "Latitude": 108.039746
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Đắk Nông",
   "address": "Trần Hưng Đạo, Phường Nghĩa Trung, Gia Nghĩa, Đăk Nông",
   "Longtitude": 12.000272,
   "Latitude": 107.697182
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Điện Biên",
   "address": "Số 48 Tổ 25  - Phường Mường Thanh - TP Điện Biên Phủ - Tỉnh Điện Biên",
   "Longtitude": 21.383412,
   "Latitude": 103.022744
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Đồng Nai",
   "address": "2 Phan Đình Phùng, Phường Quang Vinh, Thành phố Biên Hòa, Đồng Nai, Việt Nam",
   "Longtitude": 10.949385,
   "Latitude": 106.817168
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Đồng Tháp",
   "address": "05 Võ Trường Toản, Phường 1, TP. Cao Lãnh, Đồng Tháp, Việt Nam",
   "Longtitude": 10.459367,
   "Latitude": 105.633172
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Gia Lai",
   "address": "09 Trần Hưng Đạo, P.Tây Sơn, Thành phố Pleiku, Gia Lai, Việt Nam",
   "Longtitude": 13.980467,
   "Latitude": 108.003748
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hà Giang",
   "address": "số 338, đường Nguyễn Trãi, thành phố Hà Giang, P. Nguyễn Trãi, Hà Giang, Việt Nam",
   "Longtitude": 22.821372,
   "Latitude": 104.982495
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hà Nam",
   "address": "Trường Chinh, Lương Khánh Thiện, tp. Phủ Lý, Hà Nam, Việt Nam",
   "Longtitude": 20.540886,
   "Latitude": 105.917755
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hà Nội",
   "address": "Số 4 Sơn Tây, phường Điện Biên, Ba Đình, Hà Nội",
   "Longtitude": 21.033038,
   "Latitude": 105.833288
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hà Tĩnh",
   "address": "71 Hải Thượng Lãn Ông, Bắc Hà, Hà Tĩnh",
   "Longtitude": 18.343526,
   "Latitude": 105.894945
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hải Dương",
   "address": "42 Quang Trung, P. Quang Trung, Thành phố Hải Dương, Hải Dương, Việt Nam",
   "Longtitude": 20.940279,
   "Latitude": 106.333823
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hải Phòng",
   "address": "Số 38 Lê Đại Hành - quận Hồng Bàng - Thành phố Hải Phòng ",
   "Longtitude": 20.858986,
   "Latitude": 106.683762
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hậu Giang",
   "address": "6 Ngô Quyền, Phường 5, Vị Thanh, Hậu Giang, Việt Nam",
   "Longtitude": 9.792717,
   "Latitude": 105.482693
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hòa Bình",
   "address": "Trần Hưng Đạo, Sủ Ngòi, Hòa Bình, Hòa Bình.",
   "Longtitude": 20.82444,
   "Latitude": 105.354944
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Hưng Yên",
   "address": "Đường Hải Thượng Lãn Ông, P. An Tảo, TX. Hưng Yên",
   "Longtitude": 20.665084,
   "Latitude": 106.062009
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Kiên Giang",
   "address": "1 Trần Hưng Đạo, Thanh Vân, Rạch Giá, tỉnh Kiên Giang, Việt Nam",
   "Longtitude": 10.008291,
   "Latitude": 105.08449
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Kon Tum",
   "address": "71 Phan Đình Phùng, Quang Trung, Kon Tum, Việt Nam",
   "Longtitude": 14.356578,
   "Latitude": 107.99989
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Khánh Hòa",
   "address": "4 Phan Chu Trinh, Xương Huân, Thành phố Nha Trang, Khánh Hòa, Việt Nam",
   "Longtitude": 12.254409,
   "Latitude": 109.194256
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Lai Châu",
   "address": "Tổ 22 - phường Đông Phong - thành phố Lai Châu",
   "Longtitude": 22.392629,
   "Latitude": 103.481194
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Lạng Sơn",
   "address": "50 Đinh Tiên Hoàng, phường Chi Lăng, Thành phố Lạng Sơn, Lạng Sơn, Việt Nam",
   "Longtitude": 21.845833,
   "Latitude": 106.755181
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Lào Cai",
   "address": "82 Hoàng Liên, Cốc Lếu, TX.Lào Cai, Lào Cai, Việt Nam",
   "Longtitude": 22.497712,
   "Latitude": 103.967472
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Lâm Đồng",
   "address": "Tầng 2, Trung tâm hành chính tỉnh, Số 36 Trần Phú, TP. Đà Lạt ",
   "Longtitude": 11.936112,
   "Latitude": 108.431898
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Long An",
   "address": "70 Nguyễn Đình Chiểu, P.1, TX. Tân An, tỉnh Long An",
   "Longtitude": 10.536322,
   "Latitude": 106.41614
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Nam Định",
   "address": "14 Trần Nhân Tông, Thống Nhất, TP. Nam Định, Nam Định, Việt Nam",
   "Longtitude": 20.438404,
   "Latitude": 106.177106
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Ninh Bình",
   "address": "Số 18 Kim Đồng, Phường Phúc Thành, TX Ninh Bình",
   "Longtitude": 20.252121,
   "Latitude": 105.972531
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Ninh Thuận",
   "address": "01 Đường 21/8 TX. Phan Rang, Tháp Chàm",
   "Longtitude": 11.567305,
   "Latitude": 108.989293
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Nghệ An",
   "address": "Số 18 Trường Thi, TP. Vinh",
   "Longtitude": 18.670443,
   "Latitude": 105.692458
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Phú Thọ",
   "address": "Đường Trần Phú, P. Gia Cẩm, TP. Việt Trì",
   "Longtitude": 21.318578,
   "Latitude": 105.399932
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Phú Yên",
   "address": "04 Tố Hữu P. 09 Tp Tuy Hòa, tỉnh PhúYên",
   "Longtitude": 13.106496,
   "Latitude": 109.302953
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Quảng Bình",
   "address": "Số 02 Hồ Xuân Hương, P. Đồng Mỹ, TX. Đồng Hới",
   "Longtitude": 17.476391,
   "Latitude": 106.621564
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Quảng Nam",
   "address": "15 Trần Hung Đạo, Phường Tân Thạnh, Tam Kỳ, Quảng Nam, Việt Nam",
   "Longtitude": 15.575796,
   "Latitude": 108.475044
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Quảng Ninh",
   "address": "596, 501 Lê Thánh Tông, P. Bạch Đằng, Thành phố Hạ Long, Quảng Ninh, Việt Nam",
   "Longtitude": 20.95219,
   "Latitude": 107.088436
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Quảng Ngãi",
   "address": "19 Nguyễn Chánh, Trần Phú, Quảng Ngãi, Việt Nam",
   "Longtitude": 15.120894,
   "Latitude": 108.783493
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Quảng Trị",
   "address": "34 Trần Hưng Đạo, Phường 1, Đông Hà, Quảng Trị, Việt Nam",
   "Longtitude": 16.821467,
   "Latitude": 107.096237
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Sóc Trăng",
   "address": "6 Châu Văn Tiếp, Phường 2, Sóc Trăng, Việt Nam",
   "Longtitude": 9.598954,
   "Latitude": 105.971491
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Sơn La",
   "address": "48 Lò Văn Giá, P. Chiềng Lề, Sơn La, Việt Nam",
   "Longtitude": 21.344135,
   "Latitude": 103.910887
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Tây Ninh",
   "address": "22 Lê Lợi, Phường 3, Tây Ninh, Việt Nam",
   "Longtitude": 11.310411,
   "Latitude": 106.100442
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Tiền Giang",
   "address": "4 Hùng Vương, Phường 1, Thành phố Mỹ Tho, Tiền Giang.",
   "Longtitude": 10.356238,
   "Latitude": 106.36435
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế TP HCM",
   "address": "59 Nguyễn Thị Minh Khai, Phường Bến Thành, Quận 1, Hồ Chí Minh.",
   "Longtitude": 10.774085,
   "Latitude": 106.69036
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Tuyên Quang",
   "address": "1 Đinh Tiên Hoàng, P. Tân Quang, Tuyên Quang, Việt Nam",
   "Longtitude": 21.818091,
   "Latitude": 105.209398
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Thái Bình",
   "address": "239 Hai Bà Trưng, P. Đề Thám, Thái Bình.",
   "Longtitude": 20.444605,
   "Latitude": 106.338812
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Thái Nguyên",
   "address": "27 Bến Tượng, Trưng Vương, Thành phố Thái Nguyên, Thái Nguyên, Việt Nam",
   "Longtitude": 21.597929,
   "Latitude": 105.844665
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Thanh Hóa",
   "address": "101 Nguyễn Trãi- Ba Đình, TP Thanh Hoá, Thanh Hóa.",
   "Longtitude": 19.803143,
   "Latitude": 105.77362
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Thừa Thiên Huế",
   "address": "28 Lê Lợi, Vĩnh Ninh, Thành phố Huế, Thừa Thiên Huế",
   "Longtitude": 16.465896,
   "Latitude": 107.589272
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Trà Vinh",
   "address": "16A Nguyễn Thái Học, phường 1, TP Trà Vinh",
   "Longtitude": 9.939758,
   "Latitude": 106.339785
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Vĩnh Long",
   "address": "47 Lê Văn Tám, Phường 1, Vĩnh Long.",
   "Longtitude": 10.254504,
   "Latitude": 105.969368
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Vĩnh Phúc",
   "address": "số 12 đường Hai Bà Trưng, phường Đống Đa, thành phố Vĩnh Yên, tỉnh Vĩnh Phúc",
   "Longtitude": 21.308566,
   "Latitude": 105.609353
 },
 {
   "type": "maps/map-images/attp.png",
   "Name": "Sở Y tế Yên Bái",
   "address": "656 Yên Ninh, P.Yên Ninh, Thành phố Yên Bái, Yên Bái, Việt Nam",
   "Longtitude": 21.726101,
   "Latitude": 104.890153
 }];