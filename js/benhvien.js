var data = [
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "1. Bệnh viện Bạch Mai",
   "address": "78 Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.001836,
   "Latitude": 105.839956
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "2. Bệnh viện Chợ Rẫy",
   "address": "QM45+RV phường 12, Quận 5, Hồ Chí Minh.",
   "Longtitude": 10.757034,
   "Latitude": 106.659731
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "3. Bệnh viện Đa khoa Trung ương Huế",
   "address": "16 Lê Lợi, Vĩnh Ninh, Thành phố Huế, Thừa Thiên Huế, Việt Nam",
   "Longtitude": 16.462408,
   "Latitude": 107.587969
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "4. Bệnh viện Đa khoa Trung ương Thái Nguyên",
   "address": "479 Lương Ngọc Quyến, Phan Đình Phùng, Thành phố Thái Nguyên, Thái Nguyên, Việt Nam",
   "Longtitude": 21.587896,
   "Latitude": 105.83094
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "5. Bệnh viện Đa khoa Trung ương Cần Thơ",
   "address": "315 Đường Nguyễn Văn Linh, Phường An Khánh, Ninh Kiều, Cần Thơ, Việt Nam",
   "Longtitude": 10.02875,
   "Latitude": 105.755424
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "6. Bệnh viện Đa khoa Trung ương Quảng Nam",
   "address": "Tam Hiệp, Núi Thành, Quảng Nam, Việt Nam",
   "Longtitude": 15.44714,
   "Latitude": 108.625413
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "7. Bệnh viện Việt Nam - Thụy Điển Uông Bí",
   "address": "Tuệ Tĩnh, Thanh Sơn, Thành phố Uông Bí, Quảng Ninh, Việt Nam",
   "Longtitude": 21.04163,
   "Latitude": 106.752028
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "8. Bệnh viện Hữu nghị Việt Nam - Cu Ba Đồng Hới",
   "address": "Tiểu khu 10, phường Nam Lý, Thành phố Đồng Hới, Quảng Bình, Việt Nam",
   "Longtitude": 17.474126,
   "Latitude": 106.602411
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "9. Bệnh viện Hữu nghị Việt - Đức",
   "address": "40 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội 100000, Việt Nam",
   "Longtitude": 21.028489,
   "Latitude": 105.847065
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "10. Bệnh viện E",
   "address": "89 Trần Cung, Nghĩa Tân, Cầu Giấy, Nghĩa Tân Cầu Giấy Hà Nội, Việt Nam",
   "Longtitude": 21.049981,
   "Latitude": 105.789744
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "11. Bệnh viện Hữu nghị",
   "address": "1 Trần Khánh Dư, Bạch Đằng, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.015452,
   "Latitude": 105.861794
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "12. Bệnh viện Thống Nhất",
   "address": "1 Lý Thường Kiệt, Phường 7, Tân Bình, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.791341,
   "Latitude": 106.65343
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "13. Bệnh viện C Đà Nẵng",
   "address": "122 Hải Phòng, Thạch Thang, Q. Hải Châu, Đà Nẵng, Việt Nam",
   "Longtitude": 16.072579,
   "Latitude": 108.216576
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "14. Bệnh viện K",
   "address": "43 Quán Sứ, Hàng Bông, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.026896,
   "Latitude": 105.846061
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "15. Bệnh viện Nhi Trung ương",
   "address": "18/879 Đường La Thành, Láng Thượng, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.024842,
   "Latitude": 105.807607
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "16. Bệnh viện Phụ - Sản Trung ương",
   "address": "43 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.027093,
   "Latitude": 105.84682
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "17. Bệnh viện Mắt Trung ương",
   "address": "85 Phố Bà Triệu, Bùi Thị Xuân, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.017601,
   "Latitude": 105.849334
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "18. Bệnh viện Tai - Mũi - Họng Trung ương",
   "address": "78 Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 20.999646,
   "Latitude": 105.840613
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "19. Bệnh viện Nội tiết Trung ương",
   "address": "80 Ngõ 82 Yên Lãng, Láng Hạ, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.012302,
   "Latitude": 105.815072
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "20. Bệnh viện Răng - Hàm - Mặt Trung ương Hà Nội",
   "address": "40 Tràng Thi, Hàng Bông, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.027772,
   "Latitude": 105.846368
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "21. Bệnh viện Răng - Hàm - Mặt Trung ương thành phố Hồ Chí Minh",
   "address": "201A Nguyễn Chí Thanh, Phường 12, Quận 5, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.757873,
   "Latitude": 106.660513
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "22. Bệnh viện 71 Trung ương",
   "address": "Thôn Tâm Trung, Phường Quảng Tâm, Quảng Xương, TP Thanh Hoá, Việt Nam",
   "Longtitude": 19.758917,
   "Latitude": 105.842054
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "23. Bệnh viện 74 Trung ương",
   "address": "Phường Hùng Vương - Thị xã Phúc Yên - Tỉnh Vĩnh Phúc, Việt Nam",
   "Longtitude": 21.221779,
   "Latitude": 105.709536
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "24. Bệnh viện Phổi Trung ương",
   "address": "463 Hoàng Hoa Thám, Vĩnh Phúc, Ba Đình, Hà Nội, Việt Nam",
   "Longtitude": 21.042705,
   "Latitude": 105.813351
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "25. Bệnh viện Tâm thần Trung ương 1",
   "address": "Xã Hòa Bình, Huyện Thường Tín, Hà Nội, Việt Nam",
   "Longtitude": 20.869715,
   "Latitude": 105.843853
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "26. Bệnh viện Tâm thần Trung ương 2",
   "address": "Đường Nguyễn Ái Quốc, KP7, P. Tân Phong, Tp Biên Hoà, tỉnh Đồng Nai, Việt Nam",
   "Longtitude": 10.965645,
   "Latitude": 106.846086
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "27. Bệnh viện Phong - Da liễu Trung ương Quy Hòa",
   "address": "05A Chế Lan Viên, Ghềnh Ráng, TP. Quy Nhơn, Bình Định, Việt Nam",
   "Longtitude": 13.750182,
   "Latitude": 109.207852
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "28. Bệnh viện Phong - Da liễu Trung ương Quỳnh Lập",
   "address": "QL1A, Quỳnh Dị, Quỳnh Lưu, Nghệ An, Việt Nam",
   "Longtitude": 19.260773,
   "Latitude": 105.714939
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "29. Bệnh viện Điều dưỡng - Phục hồi chức năng Trung ương",
   "address": "Đường Nguyễn Du, P. Bắc Sơn, Tp. Sầm Sơn, Thanh Hoá, Việt Nam",
   "Longtitude": 19.732627,
   "Latitude": 105.897594
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "30. Bệnh viện Bệnh Nhiệt đới Trung ương",
   "address": "78 Giải Phóng, Phương Đình, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.002149,
   "Latitude": 105.83906
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "31. Bệnh viện Da liễu Trung ương",
   "address": "15A Phương Mai, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.003807,
   "Latitude": 105.838723
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "32. Bệnh viện Lão khoa Trung ương",
   "address": "1A Phương Mai, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.00397,
   "Latitude": 105.839523
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "33. Bệnh viện Y học cổ truyền Trung ương",
   "address": "29 Nguyễn Bỉnh Khiêm, Nguyễn Du, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.016032,
   "Latitude": 105.84849
 },
 {
   "type": "maps/map-images/placeholder-xs1.png",
   "Name": "34. Bệnh viện Châm cứu Trung ương",
   "address": "49 Thái Thịnh, Thịnh Quang, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.008592,
   "Latitude": 105.819378
 },
];