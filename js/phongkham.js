var dataphongkham = [
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "35. Viện Huyết học - Truyền máu Trung ương",
   "address": "Phạm Văn Bạch, Yên Hoà, Cầu Giấy, Hà Nội, Việt Nam",
   "Longtitude": 21.025095,
   "Latitude": 105.788338
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "36. Viện Vệ sinh dịch tễ Trung ương",
   "address": "1 Yec Xanh, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.013006,
   "Latitude": 105.859748
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "37. Viện Vệ sinh dịch tễ Tây Nguyên",
   "address": "24 Hai Bà Trưng, Thắng Lợi, Thành phố Buôn Ma Thuột, Đắk Lắk, Việt Nam",
   "Longtitude": 12.683004,
   "Latitude": 108.044357
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "38. Viện Pasteur Nha Trang",
   "address": "8-10 Pasteur, Xương Huân, Khanh Hoa, Khánh Hòa, Việt Nam",
   "Longtitude": 12.252146,
   "Latitude": 109.196256
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "39. Viện Pasteur thành phố Hồ Chí Minh",
   "address": "167 Pasteur, Phường 8, Quận 3, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.78623,
   "Latitude": 106.688769
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "40. Viện Dinh dưỡng (TW???)",
   "address": "48 Tăng Bạt Hổ, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.013611,
   "Latitude": 105.858969
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "41. Viện Y học Biển",
   "address": "Đại lộ Võ Nguyên Giáp, Kênh Dương, Lê Chân, Hải Phòng, Việt Nam",
   "Longtitude": 20.830404,
   "Latitude": 106.684967
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "42. Viện Sức khỏe nghề nghiệp và môi trường",
   "address": "57 Lê Quý Đôn, Phạm Đình Hổ, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.012606,
   "Latitude": 105.859675
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "43. Viện Y tế công cộng thành phố Hồ Chí Minh",
   "address": "159 Hưng Phú, Phường 8, Quận 8, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.749638,
   "Latitude": 106.678071
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "44. Viện Sốt rét - Ký sinh trùng - Côn trùng Trung ương",
   "address": "34 Trung Văn, Nam Từ Liêm, Hà Nội, Việt Nam",
   "Longtitude": 20.992263,
   "Latitude": 105.792492
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "45. Viện Sốt rét - Ký sinh trùng - Côn trùng Quy Nhơn",
   "address": "611B Nguyễn Thái Học, Nguyễn Văn Cừ, Thành phố Qui Nhơn, Bình Định, Việt Nam",
   "Longtitude": 13.757537,
   "Latitude": 109.210222
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "46. Viện Sốt rét - Ký sinh trùng - Côn trùng thành phố Hồ Chí Minh",
   "address": "699 Đường Trần Hưng Đạo, Phường 1, Quận 5 Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.753075,
   "Latitude": 106.671806
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "47. Trung tâm Truyền thông - Giáo dục sức khỏe Trung ương",
   "address": "366 Đội Cấn, Cống Vị, Ba Đình, Hà Nội, Việt Nam",
   "Longtitude": 21.037839,
   "Latitude": 105.812594
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "59. Viện Kiểm nghiệm thuốc Trung ương",
   "address": "48 Hai Bà Trưng, Tràng Tiền, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.025129,
   "Latitude": 105.850433
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "60. Viện Kiểm nghiệm thuốc thành phố Hồ Chí Minh",
   "address": "200 Cô Bắc, Phường Cô Giang, Quận 1, Hồ Chí Minh, Việt Nam",
   "Longtitude": 10.76417,
   "Latitude": 106.693781
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "61. Viện Kiểm định Quốc gia vắc xin và Sinh phẩm y tế",
   "address": "1 Nguyễn Xuân Yêm, Phường Đại Kim, Hoàng Mai, Hà Nội, Việt Nam",
   "Longtitude": 20.972211,
   "Latitude": 105.821979
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "62. Viện Kiểm nghiệm an toàn vệ sinh thực phẩm Quốc gia",
   "address": "65 Phạm Thận Duật, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam",
   "Longtitude": 21.045595,
   "Latitude": 105.778328
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "63. Viện Dược liệu (TW???)",
   "address": "3 Quang Trung, Tràng Tiền, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.02582,
   "Latitude": 105.849979
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "64. Viện Trang thiết bị và Công trình y tế",
   "address": "40 Phương Mai, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.003697,
   "Latitude": 105.836729
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "65. Viện Vắc xin và Sinh phẩm y tế",
   "address": "9 Pasteur, Phường Xương Huân, Nha Trang, Khánh Hòa, Việt Nam",
   "Longtitude": 12.25059,
   "Latitude": 109.195047
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "66. Trung tâm Nghiên cứu, sản xuất vắc xin và Sinh phẩm y tế",
   "address": "135 Phố Lò Đúc, Phường Phạm Đình Hổ, Quận Hai Bà Trưng, Hà Nội.",
   "Longtitude": 21.011675,
   "Latitude": 105.859408
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "67. Viện Pháp y Quốc gia",
   "address": "41 Nguyễn Đình Chiểu, Lê Đại Hành, Hai Bà Trưng, Hà Nội, Việt Nam",
   "Longtitude": 21.0142,
   "Latitude": 105.84694
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "68. Viện Pháp y tâm thần Trung ương",
   "address": "Hoà Binh, Thường Tín, Hà Nội, Việt Nam",
   "Longtitude": 20.870796,
   "Latitude": 105.84331
 },
 {
   "type": "maps/map-images/cckhhgd.png",
   "Name": "69. Trung tâm Điều phối Quốc gia về ghép bộ phận cơ thể người.",
   "address": "40 Tràng Thi, Hoàn Kiếm, Hà Nội.",
   "Longtitude": 21.027581,
   "Latitude": 105.846132
 },];